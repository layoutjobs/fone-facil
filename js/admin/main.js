$(document).ready(function() {
	$('#ajax-loading').bind('ajaxSend', function() {
		$('body').addClass('loading');
	}).bind('ajaxComplete', function() {
		$('body').removeClass('loading');
	});
	
	$('button[rel="tooltip"]').tooltip();	
	
	if ($.fn.mask) {
		$('[data-input-mask]').each(function() {
			that = this;
			$(that).mask($(that).attr('data-input-mask'));
		});	
	}
});