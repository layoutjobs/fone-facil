<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="pt-BR" />
	<title><?php echo Yii::app()->name; ?> | <?php echo CHtml::encode($this->pageTitle); ?></title>
 
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/admin/bootstrap.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/admin/main.css'); ?>	

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body class="<?php echo $this->id . ' ' . $this->action->id; ?>">
	
<?php if (!Yii::app()->user->isGuest) : ?>
	
<?php $this->widget('bootstrap.widgets.TbNavbar', array(
	'fixed' => 'top',
	'brand' => CHtml::encode(Yii::app()->name),
	'brandUrl' => Yii::app()->createUrl('home/index'),
	'collapse' => true,
	'type' => 'inverse',
	'items' => array(
		array(
			'class' => 'bootstrap.widgets.TbMenu',
			'items' => array(
				array(
					'label' => 'Páginas',
					'url' => array('/pagina'),
				),
				array(
					'label' => 'Usuários',
					'url' => array('/usuario'),
					'visible' => Yii::app()->user->nivelAcesso >= Usuario::NIVEL_ACESSO_ADMIN
				)
			)
		),
		array(
			'class' => 'bootstrap.widgets.TbMenu',
			'htmlOptions' => array(
				'class' => 'pull-right'
			),
			'items' => array(
				array(
					'label' => 'Ir para o site',
					'url' => Yii::app()->request->hostInfo . Yii::app()->request->baseUrl
				),
				array(
					'label' => Yii::app()->user->name,
					'items' => array(
						array(
							'label' => 'Alterar senha',
							'url' => array('/home/alterarSenha')
						),
						'---',
						array(
							'label' => 'Sair',
							'url' => array('/home/logout'),
						)						
					)
				)

			)
		)
	)
)); ?>

<?php if (!empty($this->menu)) : ?>

<div id="menu">
	<?php $this->widget('bootstrap.widgets.TbNavbar', array(
		'fixed' => 'top',
		'brand' => false,
		'items' => $this->menu
	)); ?>
</div><!-- menu -->

<?php endif; ?>	
	
<?php endif; ?>	

<div class="container" id="page">
	<div class="page-header">
		<h2><?php echo $this->pageTitle; ?> <small><?php echo $this->pageSubtitle; ?></small></h2>
	</div>
	
	<?php if(Yii::app()->user->hasFlash('success')): ?>
	<p class="flash alert alert-success"><?php echo Yii::app()->user->getFlash('success'); ?></p>
	<?php endif; ?>
	
	<?php if(Yii::app()->user->hasFlash('error')): ?>
	<p class="flash alert alert-error"><?php echo Yii::app()->user->getFlash('error'); ?></p>
	<?php endif; ?>
	
	<?php if(Yii::app()->user->hasFlash('info')): ?>
	<p class="flash alert alert-info"><?php echo Yii::app()->user->getFlash('info'); ?></p>
	<?php endif; ?>
	
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div><!-- page -->

<div id="ajax-loading"></div>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/admin/main.js'); ?>

</body>
</html>
