<?php 
$this->pageTitle = 'Páginas';
$this->menu = array(
	array(
		'class' => 'bootstrap.widgets.TbMenu',
		'items' => array(
			array(
				'label' => 'Incluir',
				'icon' => 'file',
				'url' => array('create')
			)
		)
	)
); ?>

<?php echo $this->renderPartial('_searchForm', array('model' => $model)); ?>

<div class="clearfix"></div>

<?php $this->widget('GridView', array(
	'id' => 'paginaGrid',
	'type' => 'striped bordered',
	'dataProvider' => $model->search(),
	'columns' => array(
		'titulo',
		'nome',
		array(
			'class' => 'ButtonColumn',
			'template' => '{anuncios} {update} {delete}',
			'buttons' => array(
				'anuncios' => array(
					'label' => 'Anúncios',
					'icon' => 'bullhorn',
					'url' => "CHtml::normalizeUrl(array('anuncios', 'id' => \$data->primaryKey))",
					'options' => array('class' => 'btn btn-success btn-small btn-anuncios'),
					'visible' => "count(\$data->anuncios) > 0"
				)
			)			
		)
	)
)); ?>

<?php
$js = <<<EOD
$('#paginaSearchForm').submit(function(e) {
	$.fn.yiiGridView.update('paginaGrid', {
		data:$(this).serialize()
	});
	return false;
});
EOD;

Yii::app()->clientScript->registerScript('paginaIndex', $js); ?>