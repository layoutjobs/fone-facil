<tr<?php echo $form->errorSummary($data) ? ' class="error"' : ''; ?>>
	<td><h4 style="margin: 0">#<?php echo $index + 1; ?></h4></td>
	
	<td><?php echo $form->textField($data, "[$data->id]titulo", array('class' => 'input-block-level')); ?></td>

	<td><?php echo $form->textField($data, "[$data->id]campo1Valor", array('class' => 'input-block-level')); ?></td>		
	
	<td><?php echo $form->errorSummary($data, '', '', array('class' => false)); ?></td>
	
	<td>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'url' => '#',
			'type' => 'danger',
			'icon' => 'trash white',
			'size' => 'small',
			'htmlOptions' => array(
				'rel' => 'tooltip',
				'title' => 'Excluir',
				'class' => 'btn-delete'
			)
		)); ?>				
	</td>
	<td style="display: none;" class="id"><?php echo $data->id; ?></td>
</tr>

<?php 
$url = $this->createUrl('removerAnuncio');
$js = <<<EOD
$('.list-view .btn-delete').live('click', function(e) {
	var id = $(this).parents('tr').children('td.id').text().trim();
	jQuery.ajax({
		'success': function() { 
			$.fn.yiiListView.update('paginaAnuncioTipo0List'); 
		},
		'url': '$url&id='+id,
		'cache': false
	});
	e.preventDefault();
});
EOD;
Yii::app()->clientScript->registerScript('paginaAnunciosTipo2', $js); ?>