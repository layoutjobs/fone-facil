<?php 
$this->pageTitle = 'Página';
$this->pageSubtitle = '/ Anúncios (' . CHtml::encode($model->titulo) . ')';
$this->menu = array(
	array(
		'class' => 'bootstrap.widgets.TbMenu',
		'items' => array(
			array(
				'label' => 'Voltar',
				'icon' => 'arrow-left',
				'url' => array('index'),
			),
			array(
				'label' => 'Incluir',
				'icon' => 'file',
				'url' => array('create')
			),
			array(
				'label' => 'Editar',
				'icon' => 'pencil',
				'url' => array('update', 'id' => $model->id)
			),			
			array(
				'label' => 'Excluir',
				'icon' => 'trash',
				'url' => '#',
				'linkOptions' => array(
					'submit' => array('delete', 'id' => $model->id),
					'confirm' => 'Tem certeza que deseja excluir este registro?'
				)
			)
		)
	)
); 

$tiposAnuncio = array(
	'Página Inteira', 'Padrão', 'Básico'
); 
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'anuncioForm',
	'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>

<?php foreach ($tiposAnuncio as $i => $tipoAnuncio) : ?>
	
<div class="panel">
	<div class="panel-header">
		<h3 class="panel-heading"><?php echo $tipoAnuncio; ?></h3>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'ajaxLink',
			'type' => 'success',
			'size' => 'small',
			'icon' => 'plus white',
			'url' => $this->createUrl('incluirAnuncio', array('pagina' => $model->id, 'tipo' => $i)),
			'ajaxOptions' => array(		
				'success' => "js: function() {
					// $.fn.yiiListView.update('paginaAnuncioTipo{$i}List'); 
					location.reload();
				}",		
			),
			'htmlOptions' => array(
				'title' => 'Incluir',
				'rel' => 'tooltip'
			)
		)); ?>			
	</div>

	<?php $this->widget('bootstrap.widgets.TbListView', array(
		'id' => "paginaAnuncioTipo{$i}List",
		'itemView' => "_anuncioTipo{$i}ListItem",
		'template' => '<table class="table"><tbody>{items}</tbody></table>',
		'viewData' => array('form' => $form),
		'dataProvider' => new CArrayDataProvider($anuncios[$i], array(
			'pagination' => array(
				'pageSize' => 999999
			)
		)),
	)); ?>
</div>

<?php endforeach; ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'label' => 'Salvar',
		'type' => 'primary',
		'icon' => 'ok white'
	)); ?> 
	
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'label' => 'Cancelar',
		'url' => $this->createUrl('index'),
		'type' => 'link'
	)); ?>
</div>
	
<?php $this->endWidget(); ?>