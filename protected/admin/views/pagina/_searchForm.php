<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'paginaSearchForm',
	'type' => 'search',
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
	'htmlOptions' => array('class' => 'pull-right')
)); ?>

<?php echo $form->textFieldRow($model, 's', array('class' => 'search-query')); ?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
	'icon' => 'search',
	'htmlOptions' => array(
		'data-title' => 'Pesquisar', 
		'rel' => 'tooltip'
	)
)); ?>
		
<?php $this->endWidget(); ?>