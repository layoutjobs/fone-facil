<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'paginaForm',
	'type' => 'horizontal'
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'titulo', array('class' => 'span4')); ?>

<?php echo $form->textFieldRow($model, 'nome', array('class' => 'span4')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'label' => 'Salvar',
		'type' => 'primary',
		'icon' => 'ok white'
	)); ?> 
	
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'label' => 'Cancelar',
		'url' => $this->createUrl('index'),
		'type' => 'link'
	)); ?>
</div>
	
<?php $this->endWidget(); ?>