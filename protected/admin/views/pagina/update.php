<?php 
$this->pageTitle = 'Página';
$this->pageSubtitle = '/ Editar';
$this->menu = array(
	array(
		'class' => 'bootstrap.widgets.TbMenu',
		'items' => array(
			array(
				'label' => 'Voltar',
				'icon' => 'arrow-left',
				'url' => array('index'),
			),
			array(
				'label' => 'Incluir',
				'icon' => 'file',
				'url' => array('create')
			),
			array(
				'label' => 'Anúncios',
				'icon' => 'bullhorn',
				'url' => array('anuncios', 'id' => $model->id)
			),
			array(
				'label' => 'Excluir',
				'icon' => 'trash',
				'url' => '#',
				'linkOptions' => array(
					'submit' => array('delete', 'id' => $model->id),
					'confirm' => 'Tem certeza que deseja excluir este registro?'
				)
			)
		)
	)
); ?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>