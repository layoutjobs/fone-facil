<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'usuarioForm',
	'type' => 'horizontal'
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row">
	<div class="span6">
		<?php echo $form->toggleButtonRow($model, 'ativo', array(
			'options' => array(
				'enabledLabel' => 'Sim', 
				'disabledLabel' => 'Não'
			))
		); ?>
		
		<?php echo $form->textFieldRow($model, 'nome'); ?>
		
		<?php echo $form->textFieldRow($model, 'login'); ?>		
	</div>
	<div class="span6">
		<?php echo $form->toggleButtonRow($model, 'nivelAcesso', array(
			'options' => array(
				'enabledLabel' => 'Sim', 
				'disabledLabel' => 'Não'
			))
		); ?>		

		<?php echo $form->passwordFieldRow($model, 'novaSenha'); ?>
		
		<?php echo $form->passwordFieldRow($model, 'novaSenhaRepeat'); ?>		
	</div>
</div>		
	
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'label' => 'Salvar',
		'type' => 'primary',
		'icon' => 'ok white'
	)); ?> 
	
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'label' => 'Cancelar',
		'url' => $this->createUrl('index'),
		'type' => 'link'
	)); ?>
</div>
	
<?php $this->endWidget(); ?>