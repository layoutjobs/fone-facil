<?php 
$this->pageTitle = 'Usuários';
$this->menu = array(
	array(
		'class' => 'bootstrap.widgets.TbMenu',
		'items' => array(
			array(
				'label' => 'Incluir',
				'icon' => 'file',
				'url' => array('create')
			)
		)
	)
); ?>

<?php echo $this->renderPartial('_searchForm', array('model' => $model)); ?>

<div class="clearfix"></div>

<?php $this->widget('GridView', array(
	'id' => 'usuarioGrid',
	'type' => 'striped bordered',
	'dataProvider' => $model->search(),
	'columns' => array(
		'nome',
		'login',
		array(
			'type' => 'html',
			'name' => 'nivelAcesso',
			'value' => '$data->nivelAcesso == true ? \'<i class="icon-ok"></i>\' : \'<i class="icon-ban-circle"></i>\''
		),
		array(
			'type' => 'html',
			'name' => 'ativo',
			'value' => '$data->ativo==true ? \'<i class="icon-ok"></i>\' : \'<i class="icon-ban-circle"></i>\''
		),
		array(
			'class' => 'ButtonColumn'
		)
	)
)); ?>

<?php 
$js = <<<EOD
$('#usuarioSearchForm').submit(function(e) {
	$.fn.yiiGridView.update('usuarioGrid', {
		data:$(this).serialize()
	});
	return false;
});
EOD;

Yii::app()->clientScript->registerScript('usuarioIndex', $js); ?>