<?php 
$this->pageTitle = 'Usuário';
$this->pageSubtitle = '/ Incluir';
$this->menu = array(
	array(
		'class' => 'bootstrap.widgets.TbMenu',
		'items' => array(
			array(
				'label' => 'Voltar',
				'icon' => 'arrow-left',
				'url' => array('index'),
			)
		)
	)
); ?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>