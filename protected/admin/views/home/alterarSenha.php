<?php 
$this->pageTitle = 'Usuário';
$this->pageSubtitle = '/ Alterar senha';
$this->menu = array(
	array(
		'class' => 'bootstrap.widgets.TbMenu',
		'items' => array(
			array(
				'label' => 'Voltar',
				'icon' => 'arrow-left',
				'url' => Yii::app()->user->returnUrl,
			)
		)
	)
); ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id' => 'usuarioAlterarSenhaForm',
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true
	)
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->passwordFieldRow($model, 'novaSenha', array('class' => 'span')); ?>

<?php echo $form->passwordFieldRow($model, 'novaSenhaRepeat', array('class'=>'span')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'label' => 'Salvar',
		'icon' => 'ok white',
		'type' => 'primary'
	)); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'label' => 'Cancelar',
		'type' => 'link',
		'url' => Yii::app()->user->returnUrl
	)); ?>
</div>
	
<?php $this->endWidget(); ?>