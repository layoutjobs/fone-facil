<?php $this->pageTitle = 'Login'; ?>

<?php echo CHtml::error($model, 'username', array('class' => 'alert alert-block alert-error')); ?>

<?php echo CHtml::error($model, 'password', array('class' => 'alert alert-block alert-error')); ?>				

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'loginForm',
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true
	)
)); ?>

<?php echo $form->labelEx($model, 'username'); ?>

<?php echo $form->textField($model, 'username', array('class' => 'input-block-level')); ?>

<?php echo $form->labelEx($model, 'password'); ?>

<?php echo $form->passwordField($model, 'password', array('class' => 'input-block-level')); ?>

<div class="form-actions">
 	<label class="checkbox pull-left">
    	<?php echo $form->checkBox($model, 'rememberMe'); ?>
    	<?php echo $model->getAttributeLabel('rememberMe'); ?>
	</label>
	
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'label' => 'Entrar',
		'type' => 'primary',
		'htmlOptions' => array(
			'class' => 'pull-right'
		)
	)); ?>
</div>

<?php $this->endWidget(); ?>
