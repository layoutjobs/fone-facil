<?php

class HomeController extends Controller
{
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'users' => array('@')
			),
			array(
				'allow',
				'actions' => array('error', 'login'),
				'users' => array('*')
			),
			array('deny')
		);
	}
		
	public function actionIndex()
	{
		$this->redirect(Yii::app()->createUrl('pagina'));
	}
	
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionLogin()
	{
		$model = new LoginForm;
		
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'loginForm') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		
		$this->render('login', array(
			'model' => $model
		));
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionAlterarSenha($id = null)
	{
		if (!$id) 
			$id = Yii::app()->user->id;
			
		$model = Usuario::model()->findByPk($id);
		
		if ($model === null)
			throw new CHttpException(404, 'A página solicitada não existe.');
		
		$model->setScenario('alterarSenha');
		
		if (Yii::app()->request->urlReferrer !== Yii::app()->request->hostInfo . Yii::app()->request->requestUri)
			Yii::app()->user->setReturnUrl(Yii::app()->request->urlReferrer);
		
		if (isset($_POST['Usuario'])) {
			$model->attributes = $_POST['Usuario'];
			
			if ($model->save()) {
				Yii::app()->user->setFlash('sucess', '<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		
		$this->render('alterarSenha', array('model' => $model));
	}
	
}
