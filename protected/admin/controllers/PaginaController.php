<?php

class PaginaController extends Controller
{
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'users' => array('@')
			),
			array('deny')
		);
	}
	
	public function actionIncluirAnuncio($pagina, $tipo)
	{
		$pagina = $this->loadModel($pagina);
			
		$anuncio = new Anuncio;
		$anuncio->paginaId = $pagina->id;
		$anuncio->tipo = $tipo;
		$anuncio->save();
	}
	
	public function actionRemoverAnuncio($id)
	{
		Anuncio::model()->findByPk($id)->delete();
		
		Yii::app()->end();
	}
	
	public function actionIndex()
	{
		$model = new Pagina('search');
		$model->unsetAttributes();
		
		if (isset($_GET['Pagina']))
			$model->setAttributes($_GET['Pagina']);
		
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionCreate()
	{
		$model = new Pagina;
		
		$this->performAjaxValidation($model);
	
		if (isset($_POST['Pagina'])) {
			$model->setAttributes($_POST['Pagina']);
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', '<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');
				$this->redirect(array('update', 'id' => $model->id));
			}
		}
		
		$this->render('create', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		
		$this->performAjaxValidation($model);
	
		if (isset($_POST['Pagina'])) {
			$model->setAttributes($_POST['Pagina']);
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', '<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');
				$this->refresh();
			}
		}
		
		$this->render('update', array(
			'model' => $model
		));
	}
	
	public function actionAnuncios($id)
	{
		$model = $this->loadModel($id);
		
		$this->performAjaxValidation($model);
		
		$anunciosPorTipo = array(
			0 => Anuncio::model()->findAll(array(
					'index' => 'id', 
					'condition' => 'tipo = 0 AND paginaId = :paginaId', 
					'params' => array(':paginaId' => $model->id)
				)
			),
			1 => Anuncio::model()->findAll(array(
					'index' => 'id', 
					'condition' => 'tipo = 1 AND paginaId = :paginaId', 
					'params' => array(':paginaId' => $model->id)
				)
			),
			2 => Anuncio::model()->findAll(array(
					'index' => 'id', 
					'condition' => 'tipo = 2 AND paginaId = :paginaId', 
					'params' => array(':paginaId' => $model->id)
				)
			)
		);
		
		foreach ($anunciosPorTipo as $i => $anuncios) {
			foreach ($anuncios as $ii => $anuncio) {
				$anunciosPorTipo[$i][$ii]->setScenario('tipo' . $anuncio->tipo); 	
			}
		}
		
		if (isset($_POST['Pagina'])) {
			$model->setAttributes($_POST['Pagina']);
			
			if (empty($_POST['Pagina']['participantes']))
				$model->participantes = null;
						
			if ($model->save()) {
				Yii::app()->user->setFlash('success', '<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');
				$this->refresh();
			}
		}
				
	    if (isset($_POST['Anuncio'])) {
	    	foreach ($anunciosPorTipo as $i => $anuncios) {
		        foreach($anuncios as $ii => $anuncio) {
					if (isset($_POST['Anuncio'][$ii])) {
						$uploadedFile = CUploadedFile::getInstance(Anuncio::model(), "[$ii]img");
						$anunciosPorTipo[$i][$ii]->setAttributes($_POST['Anuncio'][$ii]);
						if ($uploadedFile)
							$anunciosPorTipo[$i][$ii]->img = $uploadedFile;					
						$anunciosPorTipo[$i][$ii]->save();
					}
				}			
			}
	    }
				
		$this->render('anuncios', array(
			'model' => $model,
			'anuncios' => $anunciosPorTipo
		));
	}
	
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->loadModel($id)->delete();
			$this->redirect(array('index'));
		} else
			throw new CHttpException(400, 'Solicitação inválida. Por favor, não repita esta solicitação novamente.');
	}
	
	public function loadModel($id)
	{
		$model = Pagina::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'A página solicitada não existe.');
		return $model;
	}
	
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'paginaForm') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
