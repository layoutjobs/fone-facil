<?php

class UsuarioController extends Controller
{		
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'users' => array('*'),
				'expression' => 'isset($user->nivelAcesso) && ($user->nivelAcesso >= Usuario::NIVEL_ACESSO_ADMIN)'
			),
			array(
				'allow',
				'users' => array('alterarSenha')
			),
			array('deny')
		);
	}
	
	public function actionIndex()
	{
		$model = new Usuario('search');
		$model->unsetAttributes();
		
		if (isset($_GET['Usuario']))
			$model->setAttributes($_GET['Usuario']);
		
		$this->render('index', array(
			'model' => $model
		));
	}
	
	public function actionCreate()
	{
		$model = new Usuario;
	
		if (isset($_POST['Usuario'])) {
			$model->setAttributes($_POST['Usuario']);
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', '<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');
				$this->redirect(array('update', 'id' => $model->id));
			}
		}
		
		$this->render('create', array(
			'model' => $model
		));
	}
	
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		
		if (isset($_POST['Usuario'])) {
			$model->setAttributes($_POST['Usuario']);
			
			if ($model->save()) {
				Yii::app()->user->setFlash('success', '<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');
				$this->refresh();
			}
		}
		
		$this->render('update', array(
			'model' => $model
		));
	}
	
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->loadModel($id)->delete();
			$this->redirect(array('index'));
		} else
			throw new CHttpException(400, 'Solicitação inválida. Por favor, não repita esta solicitação novamente.');
	}
	
	public function loadModel($id)
	{
		$model = Usuario::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'A página solicitada não existe.');
		return $model;
	}
	
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'usuarioForm') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
