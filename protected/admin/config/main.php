<?php

$admin = dirname(dirname(__FILE__));
$main  = dirname($admin);
Yii::setPathOfAlias('admin', $admin);

return array(
	'basePath' => $main,
	'name' => 'FoneFácil Salto',
	'defaultController' => 'home',
	//'extensionPath' => $admin . '/extensions',
    'controllerPath' => $admin . '/controllers',
    'viewPath' => $admin . '/views',
    'runtimePath' => $admin . '/runtime',	 
	'language' => 'pt_br',
	
	// preloading components
	'preload' => array(
		'log',
		'bootstrap'
	),
	
	// autoloading model and component classes
	'import' => array(
        'admin.models.*',
        'admin.components.*',
		'admin.widgets.*',
		'application.models.*'
	),
	
	// application modules
	'modules' => array(
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'admin',
			'generatorPaths' => array(
				'bootstrap.gii'
			)
		)
	),
	
	// application components
	'components' => array(
		'user' => array(
			'allowAutoLogin' => true
		),
		'coreMessages' => array(
			// necessário para ler mensagens em /protected/messages.
			'basePath' => null
		),
		'user' => array(
			'loginUrl' => array('home/login'),
			'allowAutoLogin' => true 
		),
		'db' => array(
			'connectionString' => 'sqlite:protected/data/fonefacil.db',
		),
		'errorHandler' => array(
			'errorAction' => 'home/error'
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning'
				)
			)
		),
		'bootstrap' => array(
			'class' => 'ext.bootstrap.components.Bootstrap',
			'coreCss' => false,
			'jqueryCss' => false,
			'enableBootboxJS' => false
		)
	),
	
	'params' => require($main . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'params.php')
);