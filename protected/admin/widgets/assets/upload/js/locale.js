/*
 * jQuery File Upload Plugin Localization Example 6.5
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2012, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

window.locale = {
    "fileupload": {
        "errors": {
            "maxFileSize": "O arquivo é muito grande.",
            "minFileSize": "O arquivo é muito pequeno.",
            "acceptFileTypes": "O tipo de arquivo não permitido.",
            "maxNumberOfFiles": "O número máximo de arquivos foi excedido.",
            "uploadedBytes": "Os bytes envidados excede o tamanho do arquivo.",
            "emptyResult": "A ação solicitada retornou um resultado vazio."
        },
        "error": "Erro",
        "start": "Iniciar",
        "cancel": "Cancelar",
        "destroy": "Excluir"
    }
};
