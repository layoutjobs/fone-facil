<?php

Yii::import('bootstrap.widgets.TbButtonColumn');

class ButtonColumn extends TbButtonColumn
{
	public $template = '{update} {delete}';
	
	public $updateButtonUrl = 'Yii::app()->controller->createUrl("update", array("id" => $data->primaryKey))';
	
	public $updateButtonOptions = array('class' => 'btn btn-small btn-update');
	
	public $deleteButtonUrl = 'Yii::app()->controller->createUrl("delete", array("id" => $data->primaryKey))';
	
	public $deleteConfirmation = "js: 'Tem certeza que deseja excluir este registro?'";
	
	public $deleteButtonOptions = array('class' => 'btn btn-small btn-delete', 'icon' => 'pencil white');
}
