<?php

class Upload extends CWidget
{
	/**
	 * @var array of options to be encoded and passed to the Fileupload Javascript API.
	 */
	public $options;
		
	/**
	 * @var string the url to the upload handler.
	 */
	public $url;
	
	/**
	 * @var boolean if the gallery must be rendered and it's assets 
	 * registered. Defaults to false.
	 */
	public $renderGallery = false;	
		
	/**
	 * @var string the form view to be rendered.
	 */
	public $formView = 'upload/form';
	
	/**
	 * @var string the upload view to be rendered.
	 */
	public $uploadView = 'upload/upload';
	
	/**
	 * @var string the download view to be rendered.
	 */
	public $downloadView = 'upload/download';
	
	/**
	 * @var string the gallery view to be rendered.
	 */
	public $galleryView = 'upload/gallery';	
	
	/**
	 * Publishes the required assets.
	 */
	public function init()
	{		
		parent::init();
		$this->publishAssets();
		
		$this->id = 'fileupload-' . $this->id;
	}
	
	/**
	 * Generates the required HTML and Javascript.
	 */
	public function run()
	{
		$options = $this->options ? CJavaScript::encode($this->options) : '[]';		
	
		$js = <<<EOD
$('#{$this->id}').fileupload({
	url: '{$this->url}'
});
$('#{$this->id}').fileupload('option', {$options});
$.ajax({
    url: $('#{$this->id}').fileupload('option', 'url'),
    dataType: 'json',
    context: $('#{$this->id}')[0]
}).done(function (result) {
    $(this).fileupload('option', 'done')
        .call(this, null, {result: result});
});
EOD;

		Yii::app()->clientScript->registerScript($this->id, $js, CClientScript::POS_READY);
		
		if ($this->formView)
			$this->render($this->formView);
		
		if ($this->renderGallery && $this->galleryView)		
			$this->render($this->galleryView);			

		if ($this->uploadView)		
			$this->render($this->uploadView);

		if ($this->downloadView)		
			$this->render($this->downloadView);
	}
	
	/**
	 * Publises and registers the required CSS and Javascript.
	 * @throws CHttpException if the assets folder was not found.
	 */
	public function publishAssets()
	{
		$assets  = dirname(__FILE__) . '/assets/upload';
		$baseUrl = Yii::app()->assetManager->publish($assets);
		
		if (is_dir($assets)) {
			// The jQuery UI widget factory, can be omitted if jQuery UI is already included
			Yii::app()->clientScript->registerScriptFile($baseUrl . "/js/vendor/jquery.ui.widget.js", CClientScript::POS_END);	

			// The Templates plugin is included to render the upload/download listings
			Yii::app()->clientScript->registerScriptFile("http://blueimp.github.com/JavaScript-Templates/tmpl.min.js", CClientScript::POS_END);
			
			// The Load Image plugin is included for the preview images and image resizing functionality
			Yii::app()->clientScript->registerScriptFile("http://blueimp.github.com/JavaScript-Load-Image/load-image.min.js", CClientScript::POS_END);
			
			// The Canvas to Blob plugin is included for image resizing functionality
			Yii::app()->clientScript->registerScriptFile("http://blueimp.github.com/JavaScript-Canvas-to-Blob/canvas-to-blob.min.js", CClientScript::POS_END);

			// Bootstrap Image Gallery are not required, but included if $galleryView is not null
			if ($this->galleryView) {
				Yii::app()->clientScript->registerScriptFile("http://blueimp.github.com/Bootstrap-Image-Gallery/js/bootstrap-image-gallery.min.js", CClientScript::POS_END);
				Yii::app()->clientScript->registerCssFile("http://blueimp.github.com/Bootstrap-Image-Gallery/css/bootstrap-image-gallery.min.css");
			}

			// CSS to style the file input field as button and adjust the Bootstrap progress bars
			Yii::app()->clientScript->registerCssFile($baseUrl . '/css/jquery.fileupload-ui.css');	
							
			// The Iframe Transport is required for browsers without support for XHR file uploads
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.iframe-transport.js', CClientScript::POS_END);
			
			// The basic File Upload plugin
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.fileupload.js', CClientScript::POS_END);

			// The File Upload file processing plugin
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.fileupload-fp.js', CClientScript::POS_END);
			
			// The File Upload user interface plugin
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/jquery.fileupload-ui.js', CClientScript::POS_END);
			
			// The localization script
			// Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/locale.js', CClientScript::POS_END);	
			
			/* TODO: find a way to register these script files:

			<!-- CSS adjustments for browsers with JavaScript disabled -->
			<noscript><link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.fileupload-ui-noscript.css"></noscript>

			<!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
			<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
									
			<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
			<!--[if gte IE 8]><script src="<?php echo Yii::app()->baseUrl; ?>/js/cors/jquery.xdr-transport.js"></script><![endif]--> */
		} else {
			throw new CHttpException(500, __CLASS__ . ' - Error: Couldn\'t find assets to publish.');
		}
	}
}