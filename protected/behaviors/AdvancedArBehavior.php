<?php

Yii::import('ext.CAdvancedArBehavior');

class AdvancedArbehavior extends CAdvancedArBehavior
{
	protected function getRelations()
	{
		$relations = array();

		foreach ($this->owner->relations() as $key => $relation) 
		{
			if ($relation[0] == CActiveRecord::MANY_MANY && 
					!in_array($key, $this->ignoreRelations) &&
					$this->owner->hasRelated($key) && 
					$this->owner->$key != -1)
			{
				$info = array();
				$info['key'] = $key;
				$info['foreignTable'] = $relation[1];

					if (preg_match('/^(.+)\((.+)\s*,\s*(.+)\)$/s', $relation[2], $pocks)) 
					{
						$info['m2mTable'] = $pocks[1];
						$info['m2mThisField'] = $pocks[2];
						$info['m2mForeignField'] = $pocks[3];
					}
					else 
					{
						$info['m2mTable'] = $relation[2];
						$info['m2mThisField'] = $this->owner->tableSchema->primaryKey; // the correct is primaryKey
						$info['m2mForeignField'] = CActiveRecord::model($relation[1])->tableSchema->primaryKey;
					}
				$relations[$key] = $info;
			}
		}
		return $relations;
	}	
		
	protected function writeRelation($relation) 
	{
		$key = $relation['key'];

		if(!is_array($this->owner->$key) && $this->owner->$key != array()) 		
			$this->owner->$key = array($this->owner->$key);
		
		foreach((array)$this->owner->$key as $foreignobject)
		{
			if(!is_numeric($foreignobject) && is_object($foreignobject))
				$foreignobject = $foreignobject->{$foreignobject->tableSchema->primaryKey}; // get the pk name from the foreign table
			
			$this->execute($this->makeManyManyInsertCommand($relation, $foreignobject));
		}
	}
	
	public function makeManyManyDeleteCommand($relation) {
		return sprintf("delete from %s where %s = '%s'", // SQLite doesn't support ignore statement
				$relation['m2mTable'],
				$relation['m2mThisField'],
				$this->owner->{$this->owner->tableSchema->primaryKey}
		);
	}
}
