<?php

class FileBehavior extends CActiveRecordBehavior
{
	public $uploadBaseFolder = 'files';

	public $uploadFolder;
	
	public $uploadModelFolder;
	
	/**
	 * @var array $uploadScopes (optional). Usage:
	 * $uploadScopes = array(
	 *   'img' => array(
	 *     'folder' => 'imgs',
	 *     'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
	 *     'attribute' => 'img'
	 *   ),
	 *   'file' => array(
	 *     'folder' => 'files', 
	 *     'acceptFileTypes' => '/(\.|\/)(docx?|pdf)$/i',
	 *     'attribute' => 'file'
	 *   ),
	 *   Another scopes...
	 * )
	 */
	public $uploadScopes = array();

	public function getUploadScopeFolder($scope) {
		return !empty($this->uploadScopes[$scope]['folder']) ? $this->uploadScopes[$scope]['folder'] : '';
	}
	
	public function getUploadScopeAcceptFileTypes($scope) {
		return !empty($this->uploadScopes[$scope]['acceptFileTypes']) ? $this->uploadScopes[$scope]['acceptFileTypes'] : '/.+$/i';
	}
	
	public function getUploadScopeAttribute($scope) {
		if (!empty($this->uploadScopes[$scope]['attribute']))		
			return $this->uploadScopes[$scope]['attribute'];
	}
	
	public function getUploadScopeAttributeValue($scope) {
		if ($this->getUploadScopeAttribute($scope) && 
			$this->owner->__isset($this->getUploadScopeAttribute($scope)))		
				return $this->owner->__get($this->getUploadScopeAttribute($scope));
	}

	public function getUploadBasePath($scope = null)
	{
		$baseUploadPath  = Yii::app()->getBasePath() . '/../' . $this->uploadBaseFolder . '/';
		$baseUploadPath .= $this->uploadFolder ?  $this->uploadFolder . '/' : '';
		$baseUploadPath .= $scope ? $this->getUploadScopeFolder($scope) . '/' : '';
		return $baseUploadPath;
	}
	
	public function getUploadBaseUrl($scope = null)
	{
		$baseUploadUrl  = Yii::app()->request->getBaseUrl() . '/' . $this->uploadBaseFolder . '/';
		$baseUploadUrl .= $this->uploadFolder ?  $this->uploadFolder . '/' : '';
		$baseUploadUrl .= $scope ? $this->getUploadScopeFolder($scope) . '/' : '';
		return $baseUploadUrl;	
	}
	
	public function getUploadModelFolder($version = null)
	{
		$fileFolder  = $this->uploadModelFolder;
		$fileFolder .= $this->uploadModelFolder && $version ? '/' : '';
		$fileFolder .= $version;
		
		return $fileFolder;
	}
	
	public function getUploadPath($scope = null, $version = null) {	
		return $this->getUploadBasePath($scope) . ($this->getUploadModelFolder($version) ? $this->getUploadModelFolder($version) . '/' : '');
	}
	
	public function getUploadUrl($scope = null, $version = null) {
		return $this->getUploadBaseUrl($scope) . ($this->getUploadModelFolder($version) ? $this->getUploadModelFolder($version) . '/' : '');
	}
	
	public function getFile($scope, $version = null, $renderOptions = array())
	{
		$url  = $this->getUploadUrl($scope, $version);
		$path = $this->getUploadPath($scope, $version);
		
		if (is_file($path . $this->getUploadScopeAttributeValue($scope))) {
				$file         = new stdClass;
				$file->name   = CHtml::encode($this->getUploadScopeAttributeValue($scope));
				$file->url    = $url . $this->getUploadScopeAttributeValue($scope);
				$file->path   = $path . $this->getUploadScopeAttributeValue($scope);
				if (!empty($renderOptions['tag'])) {
					if ($renderOptions['tag'] == 'img')
						$file->render = CHtml::image($file->url, $file->name, $renderOptions);
					else
						$file->render = CHtml::tag($renderOptions['tag'], $renderOptions, $file->name);
				}
				else
					$file->render = $file->name;
				
				return $file;
		}
	}
	
	public function getFiles($scope = null, $version = null, $renderOptions = array())
	{	
		$url  = $this->getUploadUrl($scope, $version);
		$path = $this->getUploadPath($scope, $version);
			
		$files = $this->_uploadListFiles($scope, $version);
		
		if ($files) {
			foreach ($files as $file) {				
				$fileObj         = new stdClass;
				$fileObj->name   = $file;
				$fileObj->url    = $url . $file;
				$fileObj->path   = $path . $file;
				if (!empty($renderOptions['tag'])) {
					if ($renderOptions['tag'] == 'img')
						$fileObj->render = CHtml::image($fileObj->url, $fileObj->name, $renderOptions);
					else
						$fileObj->render = CHtml::tag($renderOptions['tag'], $renderOptions, $fileObj->name);
				}
				else
					$fileObj->render = $fileObj->name;
				$return[] = $fileObj;
			}
			
			return $return;
		}
	}
	
	public function afterDelete($event)
	{
		$scopes = array_keys($this->uploadScopes);
		
		foreach ($scopes as $scope) {
			$this->_uploadDeleteFiles($this->getUploadPath($scope));
		}
		
		$this->_uploadDeleteFiles($this->getUploadPath());
		
		parent::afterDelete($event);
		return true;
	}
	
	private function _uploadListFiles($scope = null, $version = null)
	{	
		$foundFiles = glob($this->getUploadPath($scope, $version) . '*');
		
		if ($foundFiles) {
			foreach ($foundFiles as $file) {
				if (!is_dir($file) && preg_match($this->getUploadScopeAcceptFileTypes($scope), $file))
					$files[] = str_replace('/', '', str_replace(dirname($file), '', $file));
			}	
			
			return !empty($files) ? $files : array();
		} else
			return array();
	}
	
	private function _uploadDeleteFiles($dir) {
		if (is_dir($dir)) { 
			foreach(glob($dir . '/*') as $file) { 
				if(is_dir($file)) $this->_uploadDeleteFiles($file); else unlink($file); 
			} 
			rmdir($dir);
		}
	}
}
