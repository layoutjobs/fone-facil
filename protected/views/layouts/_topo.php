<div id="container-topo">
	<div id="container">
		<div class="topo-esquerdo">
			<!-- inicio menu superior -->
			
			<?php $this->widget('application.widgets.Menu', array(
				'items' => array(
					array(
						'label' => 'Conheça o FoneFácil',
						'url' => Yii::app()->createUrl('/site/conheca'),
						'itemOptions' => array('class' => 'first')
					),
					array(
						'label' => 'Quero Anunciar',
						'url' => Yii::app()->createUrl('/site/anuncie')
					),
					array(
						'label' => 'Cadastre sua empresa',
						'url' => Yii::app()->createUrl('/site/cadastro')
					),
					array(
						'label' => 'Contato',
						'url' => Yii::app()->createUrl('/site/contato'),
						'itemOptions' => array('class' => 'last')
					)
				),
				'htmlOptions' => array('class' => 'menu')
			)); ?>

			<!-- final menu superior -->
			<div class="clear"></div>
				<div class="conteudo-topo">
					<h1><img class="img-1" src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo.jpg" title="FONEFÁCIL - Catálogo Comercial de Salto" alt="FONEFÁCIL - Catálogo Comercial de Salto" /></h1>
						<!-- inicio da pesquisa -->
						<div class="pesquisa">
							<small>Escolha o Ramo de Atividade</small>

							<?php echo CHtml::dropDownList('search-pagina', 
								$this->action->id === 'pagina' ? $this->createUrl('pagina', array('id' => Yii::app()->request->getParam('id'))) : '', 
									CHtml::listData(Pagina::model()->findAll(array('order' => 'nome')), 'url', 'titulo'), 
										array('empty' => 'busca rápida')); ?>

							<p>Encontre o profissional ou comércio que <br /> procura de maneira rápida e simples.</p>
							<img class="img-3" src="<?php echo Yii::app()->request->baseUrl; ?>/img/icone-busca.png" />
						</div>
						<!-- final da pesquisa -->
					<div class="clear"></div>
				</div>
			<div class="clear"></div>
				<!-- inicio do menu inferior -->
				<?php $this->widget('application.widgets.Menu', array(
					'items' => array(
						array(
							'label' => 'Índice Anunciante',
							'url' => Yii::app()->createUrl('/site/indice'),
							'itemOptions' => array('class' => 'first')
						),
						array(
							'label' => 'Telefone Úteis',
							'url' => Yii::app()->createUrl('/site/uteis')
						),
						array(
							'label' => 'Guia Unimed',
							'url' => Yii::app()->createUrl('#'),
							'itemOptions' => array('class' => 'last')
						)
					),
					'htmlOptions' => array('class' => 'menu menu-inf')
				)); ?>
				<!-- final do menu inferior -->
		</div>
		<!-- final do conteudo do topo esquerdo-->
		<!-- inicio do conteudo topo direito-->
		<div class="topo-direito">
			<img class="img-2" src="<?php echo Yii::app()->request->baseUrl; ?>/img/topo02.png" />
		</div>
		<!-- final do conteudo direito -->
	</div>
	<div class="clear"></div>
</div>	