<!DOCTYPE html>
<html lang="pt-br">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pt-br"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="pt-br">        <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="pt-br">               <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br">                      <!--<![endif]-->
<head>
	<meta charset="utf-8">	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php echo CHtml::encode(Yii::app()->name) . ($this->pageTitle ? ' / ' . CHtml::encode($this->pageTitle) : ''); ?></title>
  <meta name="description" content="<?php echo Yii::app()->params['metaDescription']; ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-57307723-1', 'auto');
	ga('send', 'pageview');
	</script>
	
	<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

</head>
<body class="page-<?php echo $this->action->id; ?>">