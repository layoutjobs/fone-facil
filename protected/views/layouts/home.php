<?php $this->renderPartial('/layouts/_header'); ?>

<div id="principal">
	<?php $this->renderPartial('/layouts/_topo'); ?>		
</div>

<div id="content">
	<?php echo $content; ?>					
</div>
	
<?php $this->renderPartial('/layouts/_footer'); ?>