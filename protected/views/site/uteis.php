<?php $this->pageTitle = 'Telefones Úteis'; ?>

<h2>Telefones Úteis</h2>

<div id="container-conteudo">
	<div class="chamada">
		<strong>
			<div class="uteis">
				<p>AUXÍLIO A LISTA</p>
				<p>HORA CERTA</p>
				<p>PREVISÃO DO TEMPO</p>
				<p>DESPERTADOR</p>
				<p>GUARDA CIVIL MUNICIPAL</p>	
			</div>
			<div class="uteis-2">
				<p>102</p>
				<p>130</p>
				<p>132	</p>
				<p>134</p>
				<p>153</p>
			</div>
			<div class="uteis">
				<p>POLÍCIA MILITAR</p>
				<p>PRONTO SOCORRO</p>
				<p>BOMBEIROS</p>
				<p>DISQUE DENÚNCIA</p>
				<p>DEFESA CIVIL</p>
			</div>			
			<div class="uteis-2">			
				<p>190</p>
				<p>192</p>
				<p>193</p>
				<p>197</p>
				<p>199</p>
			</div>
			<div class="clear"></div>
		</strong>
	</div>
	<strong>HOSPITAIS E POSTOS</strong>
	<div class="clear"></div>
	<div class="uteis">
		<p>Centro Saúde II</p>
		<p>Centro Saúde II</p>
		<p>Clínica Salto Saúde - Nações</p>
		<p>Hospital Municipal</p>
		<p>Hospital Unimed</p>
		<p>Posto de Saúde Bela Vista</p>
		<p>Posto Saúde Adriano Randi</p>
		<p>Posto Saúde Antonio L.V.</p>
		<p>Posto Saúde Caic</p>
		<p>Posto Saúde Cecap</p>
		<p>Posto Saúde Jd. Donalísio</p>
		<p>Posto Saúde Jd. Marília</p>
		<p>Posto Saúde Jd. Saltense</p>
		<p>Posto Saúde Nações</p>
		<p>Posto Saúde S.Cruz</p>
		<p>Posto Saúde Salto São José</p>
		<p>Pronto Socorro de Fraturas</p>
		<p>Pronto Socorro de Fraturas</p>
	</div>
	<div class="uteis-3">
		<p>4021-1169</p>
		<p>4028-2514</p>
		<p>4028-6742</p>
		<p>4602-9000</p>
		<p>4029-6000</p>
		<p>4028-2547</p>
		<p>4028-2548</p>
		<p>4028-2542</p>
		<p>4029-0590</p>
		<p>4028-2542</p>
		<p>4028-2548</p>
		<p>4029-8819</p>
		<p>4028-2534</p>
		<p>4028-2554</p>
		<p>4029-8820</p>
		<p>4028-2541</p>
		<p>4029-4371</p>
		<p>4028-3443</p>
	</div>
	<div class="clear"></div>	
	<br />
	<strong>IGREJAS</strong>
	<div class="clear"></div>
	<div class="uteis">		
		<p>Congregação Cristã no Brasil</p>
		<p>Congregação Cristã no Brasil</p>
		<p>Congregação Cristã no Brasil</p>
		<p>Igr. Ev. Pent. Brasil para Cristo</p>
		<p>Igr. Ev. Pent. Brasil para Cristo</p>
		<p>Igreja de Santana e São Joaquim</p>
		<p>Igreja Evangelho Quadrangular</p>
		<p>Igreja Evangélica de Pinheiros</p>
		<p>Igreja Prebiteriana Renovada</p>
		<p>Igreja Presbiteriana</p>
		<p>Igreja Universal</p>
		<p>Paróquia de Cristo Rei</p>
		<p>Paróquia N. S. Monte Serrat</p>
		<p>Paróquia N. S. Monte Serrat</p>
		<p>Paróquia N. S. Aparecida</p>
		<p>Paróquia S. Benedito</p>
		<p>Paróquia S. Roque</p>
	</div>
	<div class="uteis-3">	
		<p>4028-3506</p>
		<p>4021-1462</p>
		<p>4029-0680</p>
		<p>4021-4440</p>
		<p>4021-4550</p>
		<p>4602-4129</p>
		<p>4029-8514</p>
		<p>4028-1028</p>
		<p>4021-2743</p>
		<p>4028-0804</p>
		<p>4602-4129</p>
		<p>4028-7213</p>
		<p>4028-1582</p>
		<p>4029-5297</p>
		<p>4028-7381</p>
		<p>4029-4707</p>
		<p>4021-2119</p>
	</div>
	<div class="clear"></div>	
	<br />
	<strong>IGREJAS</strong>
	<div class="clear"></div>
	<div class="uteis">	
		<p>Assessoria - Gabinete</p>
		<p>Câmara Municipal</p>
		<p>Coleta Seletiva</p>
		<p>Compras</p>
		<p>Comunicação e Imprensa</p>
		<p>Conservatório</p>
		<p>Contabilidade</p>
		<p>Dep. Trânsito</p>
		<p>Est. Trat. àgua - Bela Vista</p>
		<p>Habitação</p>
		<p>Patrimônio</p>
		<p>Pessoal</p>
		<p>Prefeitura PABX</p>
		<p>Protocolo</p>
		<p>Secretaria àgua e Esgosto - SAE</p>
		<p>Secretaria Criança/Adolecente</p>
		<p>Secretaria Cultura/Turismo</p>
		<p>Secretaria da Administração</p>
		<p>Secretaria da Educação</p>
		<p>Secretaria da Educação</p>
		<p>Secretaria da Fazenda</p>
		<p>Secretaria da Fazenda</p>
		<p>Secretaria da Saúde</p>
		<p>Secretaria de Esportes</p>
		<p>Secretaria de Obras</p>
		<p>Secretaria Ind./Com.</p>
		<p>Secretaria Neg. Jurídicos</p>
		<p>Secretaria Urbanismo/Planej.</p>
		<p>Tesouraria</p>
		<p>Tributação</p>
		<p>Veículos</p>
		<p>Vigilância Sanitária</p>
	</div>
	<div class="uteis-3">		
		<p>4602-8550</p>
		<p>4602-8300</p>
		<p>4602-8518</p>
		<p>4602-8528</p>
		<p>4602-8539</p>
		<p>4029-3014</p>
		<p>4602-8568</p>
		<p>4602-8514</p>
		<p>4029-2377</p>
		<p>4602-8519</p>
		<p>4602-8509</p>
		<p>4602-8520</p>
		<p>4602-8500</p>
		<p>4602-8538</p>
		<p>4029-6166</p>
		<p>4028-0823</p>
		<p>4029-4718</p>
		<p>4602-8534</p>
		<p>4602-8504</p>
		<p>4602-8526</p>
		<p>4602-8557</p>
		<p>4602-8555</p>
		<p>4028-1414</p>
		<p>4029-3025</p>
		<p>4602-1244</p>
		<p>4602-8532</p>
		<p>4602-8523</p>
		<p>4602-8542</p>
		<p>4602-8566</p>
		<p>4602-8549</p>
		<p>4028-2592</p>
		<p>4602-8906</p>
	</div>
	<div class="clear"></div>	
	<br />
	<strong>ÚTEIS</strong>
	<div class="clear"></div>
	<div class="uteis">	
		<p>1º Distrito Policial</p>
		<p>2º Distrito Policial</p>
		<p>2º Tabelião Notas e Ofício</p>
		<p>2º Tabelião Notas e Ofício</p>
		<p>3º Cia. Polícia Militar</p>
		<p>3º Cia. Polícia Militar</p>
		<p>Biblioteca Municipal</p>
		<p>Cartório Eleitoral</p>
		<p>Cartório Registro Civil</p>
		<p>Cartório Registro Civil</p>
		<p>Cartório Registro Imóveis</p>
		<p>Cartório Registro Imóveis</p>
		<p>Cemitério da Saudade</p>
		<p>Cemitério Jardim do Éden</p>
		<p>Cine Clube de Salto</p>
		<p>Conselho Tutelar</p>
		<p>Corpo de Bombeiros</p>
		<p>Correios Agência Centro</p>
		<p>Correios Agência Shopping Center</p>
		<p>Correios Sedex</p>
		<p>Delegacia Defesa da Mulher</p>
		<p>Delegacia Polícia Salto</p>
		<p>Delegacia Polícia Salto</p>
		<p>Fórum da Comarca</p>
		<p>Funerária Saltense</p>
		<p>Funerária Saltense</p>
		<p>Guarda Municipal</p>
		<p>INSS</p>
		<p>INSS</p>
		<p>Justiça do Trabalho</p>
		<p>Justiça do Trabalho</p>
		<p>Ministério Público Estado S.P.</p>
		<p>Ministério Público Estado S.P.</p>
		<p>Museu</p>
		<p>OAB</p>
		<p>Shopping Center de Salto</p>
		<p>Terminal Rodoviário</p>
		<p>Tigre Lotérico</p>
		<p>Tigre Lotérico	</p>
	</div>
	<div class="uteis-3">	
		<p> 4029-3454</p>
		<p>4028-1919</p>
		<p>4021-0767</p>
		<p>4029-5858</p>
		<p>4021-4542</p>
		<p>4029-2497</p>
		<p>4028-2575</p>
		<p>4028-2588</p>
		<p>4029-0784</p>
		<p>4029-0784</p>
		<p>4028-1783</p>
		<p>4029-3934</p>
		<p>4029-4552</p>
		<p>4029-5678</p>
		<p>4029-0671</p>
		<p>4028-2584</p>
		<p>4029-5081</p>
		<p>4029-3910</p>
		<p>4029-6659</p>
		<p>4029-7722</p>
		<p>4029-2533</p>
		<p>4021-4541</p>
		<p>4029-2233</p>
		<p>4029-2255</p>
		<p>4029-3240</p>
		<p>4029-4213</p>
		<p>4029-3031</p>
		<p>4029-2511</p>
		<p>4029-6616</p>
		<p>4021-2385</p>
		<p>4029-5244</p>
		<p>4021-1180</p>
		<p>4029-5154</p>
		<p>4029-3473</p>
		<p>4028-2472</p>
		<p>4028-1561</p>
		<p>4028-0667</p>
		<p>4028-0311</p>
		<p>4028-1087</p>
	</div>
	<div class="clear"></div>
</div>
