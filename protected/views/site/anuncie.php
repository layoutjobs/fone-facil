<?php $this->pageTitle = 'Quero Anunciar'; ?>

<h2>Quero Anunciar</h2>

<div id="container-conteudo">
	<?php if(Yii::app()->user->hasFlash('contatoForm')): ?>
	
	<p class="flash-success">
		<?php echo Yii::app()->user->getFlash('contatoForm'); ?>
	</p>
	
	<?php else: ?>
	
	<p class="chamada">Para anunciar no FoneFácil edição 
	<?php echo date('Y'); ?>, preencha os campos abaixo e aguarde o contato <br />
	de nossos representantes quando as vendas dos espaços publicitários forem iniciadas.</p>
	
	<?php $form = $this->beginWidget('CActiveForm'); ?>
	
	<?php echo $form->labelEx($model, 'nome'); ?>
	<?php echo $form->textField($model, 'nome'); ?>

	<div class="clear"></div>

	<?php echo $form->labelEx($model, 'area'); ?>
	<?php echo $form->textField($model, 'area'); ?>
	
	<div class="clear"></div>

	<?php echo $form->labelEx($model, 'contato'); ?>
	<?php echo $form->textField($model, 'contato'); ?>
	
	<div class="clear"></div>

	<?php echo $form->labelEx($model, 'telefone'); ?>
	<?php echo $form->textField($model, 'telefone'); ?>
	
	<div class="clear"></div>

	<?php echo $form->labelEx($model, 'email'); ?>
	<?php echo $form->textField($model, 'email'); ?>
	
	<div class="clear"></div>

	<?php echo CHtml::submitButton('Enviar', array('class' => 'botao')); ?>

	<?php $this->endWidget(); ?>
	
	<?php endif; ?>
</div>