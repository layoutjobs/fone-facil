<?php $this->pageTitle = 'Contato'; ?>

<h2>Contato</h2>

<div id="container-conteudo">
	<?php if(Yii::app()->user->hasFlash('contatoForm')): ?>
	
	<p class="flash-success">
		<?php echo Yii::app()->user->getFlash('contatoForm'); ?>
	</p>
	
	<?php else: ?>
	
	<p class="chamada">Esse é um espaço para você tirar suas dúvidas e fazer qualquer sugestão ou crítica.<br />
	Para isso, preencha o formulário abaixo e em breve entraremos em contato.</p>

	<p><strong>Fones:</strong> 11 4021.4421 / 11 4456.6959<br />
	<strong>Financeiro:</strong> <a href="mailto:eliana@layoutnet.com.br">eliana@layoutnet.com.br</a><br />
	<strong>Propaganda:</strong> <a href="mailto:claudio@layoutnet.com.br">claudio@layoutnet.com.br</a><br />
	<strong>Layout:</strong> Rua Henrique Viscardi, 1303 - Jd. Celani - Salto/SP</p>

	<br />
	
	<?php $form = $this->beginWidget('CActiveForm'); ?>
	
	<?php echo $form->labelEx($model, 'nome'); ?>
	<?php echo $form->textField($model, 'nome'); ?>

	<div class="clear"></div>

	<?php echo $form->labelEx($model, 'contato'); ?>
	<?php echo $form->textField($model, 'contato'); ?>
	
	<div class="clear"></div>

	<?php echo $form->labelEx($model, 'telefone'); ?>
	<?php echo $form->textField($model, 'telefone'); ?>
	
	<div class="clear"></div>

	<?php echo $form->labelEx($model, 'email'); ?>
	<?php echo $form->textField($model, 'email'); ?>
	
	<div class="clear"></div>

	<?php echo $form->labelEx($model, 'mensagem'); ?>
	<?php echo $form->textArea($model, 'mensagem'); ?>
	
	<div class="clear"></div>

	<?php echo CHtml::submitButton('Enviar', array('class' => 'botao')); ?>

	<?php $this->endWidget(); ?>
	
	<?php endif; ?>
</div>