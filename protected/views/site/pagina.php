<?php $this->pageTitle = CHtml::encode($pagina->titulo); ?>

<h2><?php echo $this->pageTitle; ?></h2>

<div id="container-conteudo">
	<!-- propaganda menor -->
	
		<?php foreach ($anuncios as $anuncio) : ?>
			<?php if ($anuncio->tipo == 1) : ?>	
			<div class="propaganda-menor">		
				<?php echo CHtml::image($anuncio->imgUrl, null); ?>
				<div class="segura-texto">
					<h3><?php echo CHtml::encode($anuncio->titulo); ?></h3>
					<p><?php echo $anuncio->conteudo; ?></p>
					<br />
					<ul>
						<?php if ($anuncio->campo1Titulo && $anuncio->campo1Valor) : ?>
							<li><strong><?php echo CHtml::encode($anuncio->campo1Titulo); ?>:</strong> <?php echo CHtml::encode($anuncio->campo1Valor); ?></li>
						<?php endif; ?>
						<?php if ($anuncio->campo2Titulo && $anuncio->campo2Valor) : ?>
							<li><strong><?php echo CHtml::encode($anuncio->campo2Titulo); ?>:</strong> <?php echo CHtml::encode($anuncio->campo2Valor); ?></li>
						<?php endif; ?>
						<?php if ($anuncio->campo1Titulo && $anuncio->campo3Valor) : ?>
							<li><strong><?php echo CHtml::encode($anuncio->campo3Titulo); ?>:</strong> <?php echo CHtml::encode($anuncio->campo3Valor); ?></li>
						<?php endif; ?>
						<?php if ($anuncio->campo1Titulo && $anuncio->campo4Valor) : ?>
							<li><strong><?php echo CHtml::encode($anuncio->campo4Titulo); ?>:</strong> <?php echo CHtml::encode($anuncio->campo4Valor); ?></li>
						<?php endif; ?>
						<?php if ($anuncio->campo1Titulo && $anuncio->campo5Valor) : ?>
							<li><strong><?php echo CHtml::encode($anuncio->campo5Titulo); ?>:</strong> <?php echo CHtml::encode($anuncio->campo5Valor); ?></li>
						<?php endif; ?>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<?php endif; ?>	
		<?php endforeach; ?>			
	
	
	<!-- propaganda maior -->
	
		<?php foreach ($anuncios as $anuncio) : ?>
			<?php if ($anuncio->tipo == 0 && $anuncio->img) : ?>	
			<div class="propaganda-maior">
				<?php echo CHtml::image($anuncio->imgUrl, null); ?>
			</div>	
			<?php endif; ?>	
		<?php endforeach; ?>
		
	
	<!-- inicio agenda -->
	<div class="lista">
		<ul>
			<?php foreach ($anuncios as $anuncio) : ?>
				<?php if ($anuncio->tipo == 2) : ?>						
					<li>
						<?php echo $anuncio->isDestaque() ? '<strong>' : ''; ?>
						<h4><?php echo CHtml::encode($anuncio->titulo); ?></h4>
						<span><?php echo CHtml::encode($anuncio->campo1Valor); ?></span>
						<?php echo $anuncio->isDestaque() ? '</strong>' : ''; ?>
					</li>
				<?php endif; ?>	
			<?php endforeach; ?>			
		</ul>
	</div>				
</div>	
