<?php $this->pageTitle = 'Índice Anunciates'; ?>

<h2>Índice Anunciates</h2>

<div id="container-conteudo">
	<p class="chamada">AGRADECEMOS PELA CONFIANÇA E POR ACREDITAREM EM NOSSO TRABALHO. 
	FICAMOS SATISFEITOS EM SABER QUE O FONEFÁCIL PODE CONTRIBUIR COM O CRESCIMENTO DE 
	SUA EMPRESA E DE NOSSA CIDADE. MUITO OBRIGADO.</p>
	
	<ul class="anunciantes">
		<?php foreach ($paginas as $pagina) : ?>
			<li><?php echo CHtml::link($pagina->titulo, $pagina->url) ; ?></li>	
		<?php endforeach; ?>	
	</ul>
</div>