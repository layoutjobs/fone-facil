<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'searchForm',
	'type' => 'search',
	'action' => !empty($action) ? $action : Yii::app()->createUrl($this->route),
	//'method' => 'get'
)); ?>

<?php echo $form->textFieldRow($model, 's', array('class' => 'search-query')); ?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
	'label' => 'OK'
)); ?>

<?php $this->endWidget(); ?>