<?php $this->pageTitle = 'Conheça o FoneFácil'; ?>

<h2>Conheça o FoneFácil</h2>

<img class="img-2" src="<?php echo Yii::app()->request->baseUrl; ?>/img/capa_2010.jpg" />

<div id="container-conteudo">
	<p class="chamada">Criado para divulgar Salto para Salto e estimular seu crescimento, o FoneFácil é um instrumento indispensável para cotações, disque-entregas e buscas em geral. Anunciando no FoneFácil,sua empresa é divulgada no catálogo impresso e na internet. FoneFácil, o Catálogo Oficial da ACIAS.</p>
	<strong>Como funciona</strong>
	<p>O FoneFácil possui as páginas divididas por ramo de atividade e seus anúncios são apresentados de maneira clara, objetiva e padronizada. Além disso, o catálogo conta com índice completo e por anunciante, que auxilia as consultas. </p>
	<br />
	<strong>Conteúdo</strong>
	<p>São mais de 2.500 empresas e profissionais, com seus telefones organizados e atualizados, telefones úteis, horários de ônibus, guia de médicos Unimed e muito mais.</p>
	<br />
	<strong>Tiragem</strong>
	<p>A tiragem do catálogo é de 20.000 exemplares e a distribuição é gratuita.</p>
	<br />
	<strong>Quem Produz</strong>
	<p>O FoneFácil é produzido pela Agência Layout e editado pela Editora Minelli Brasil.</p>
	<br />
	<strong>Apoio</strong>
	<p>As empresas abaixo apóiam a edição 2010 do FoneFácil:</p>
	<img class="img-4" src="<?php echo Yii::app()->request->baseUrl; ?>/img/logos_apoio.jpg" />
</div>
