<?php

return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'FoneFácil ' . date('Y') . ' - O primeiro e Único Catálogo Comercial de Salto' ,
	'language' => 'pt_br',
	
	// preloading components
	'preload' => array(
		'log',
		'bootstrap'
	),
	
	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.components.*'
	),
	
	// application components
	'components' => array(
		'user' => array(
			'allowAutoLogin' => true
		),
		'coreMessages' => array(
			// necessário para ler mensagens em /protected/messages.
			'basePath' => null
		),
		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false, // necessário .htaccess
			//'rules' => array(
				// '<controller:\w+>/<id:\d+>' => '<controller>/view',
				// '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				// '<controller:\w+>/<action:\w+>' => '<controller>/<action>'
			//)
		),
		'db' => array(
			'connectionString' => 'sqlite:protected/data/fonefacil.db',
		),
		'errorHandler' => array(
			'errorAction' => 'site/error'
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning'
				)
			)
		)
	),
	
	'params' => require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'params.php')
);