<?php

$envParams = require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'params-' . (strpos($_SERVER['HTTP_HOST'], '.') ? 'web' : 'local') . '.php');

return CMap::mergeArray(array(
	'emailContato' => 'contato@fonefacilsalto.com.br',
	'metaDescription' => 'FoneFácil - O primeiro e Único Catálogo Comercial de Salto.',
), $envParams);