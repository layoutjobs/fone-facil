<?php

class UserIdentity extends CUserIdentity
{
	private $_id;
	
	public function authenticate()
	{
		$usuario = Usuario::model()->find('LOWER(login)=?', array(strtolower($this->username)));
		
		if ($usuario === null)
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else if (!$usuario->validarSenha($this->password))
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else {
			$this->_id = $usuario->id;
			$this->username = $usuario->nome;
			$this->setState('nivelAcesso', $usuario->nivelAcesso);
			$this->errorCode = self::ERROR_NONE;
		}
		return $this->errorCode == self::ERROR_NONE;
	}
	
	public function getId()
    {
        return $this->_id;
    }
}