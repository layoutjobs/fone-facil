<?php

class SiteController extends Controller 
{
	public function actionIndex() 
	{				
		$this->render('index');
	}
	
	public function actionIndice() 
	{
		$paginas = Pagina::model()->findAll(array('order' => 'titulo'));
		
		$this->render('indice', array(
			'paginas' => $paginas
		));
	}
		
	public function actionPagina($id) 
	{
		$pagina = Pagina::model()->findByAttributes(array('nome' => $id));
		
		if ($pagina === null)
			throw new CHttpException(404, 'A página solicitada não existe.');

		$anuncios = Anuncio::model()->with(array(
			'pagina' => array(
				'condition' => 'nome = :nome',
				'params' => array(
					':nome' => $pagina->nome
				)
			)
		))->findAll(array('order' => 'nome'));		
		
		$this->render('pagina', array(
			'pagina' => $pagina,
			'anuncios' => $anuncios
		));
	}
	
	public function actionConheca() 
	{
		$this->render('conheca');
	}
	
	public function actionUteis() 
	{
		$this->render('uteis');
	}
	
	public function actionAnuncie() 
	{		
		$model = new ContatoForm('anuncie');
		if(isset($_POST['ContatoForm']))
		{
			$model->attributes=$_POST['ContatoForm'];
			if($model->validate())
			{
				$from = Yii::app()->params['emailContato'];
				$headers  = "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=utf-8\r\n";
				$headers .= "From: {$from}\r\n";
				$headers .= "Reply-To: {$model->email}\r\n";
				$headers .= "Return-Path: {$from}\r\n";
				$body  = "<style>";
				$body .= "table { width: 100%; font: normal 13px/20px Arial, sans-serif; }";
				$body .= "th, td { padding: 4px 8px; border-bottom: 1px solid #ddd; }";
				$body .= "th { width: 140px; text-align: left; }";
				$body .= "</style>";
				$body .= "<table>";
				$body .= "<tr>";
				$body .= "<th>{$model->getAttributeLabel('nome')}:</th>";
				$body .= "<td>{$model->nome}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('area')}:</th>";
				$body .= "<td>{$model->area}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('contato')}:</th>";
				$body .= "<td>{$model->contato}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('telefone')}:</th>";
				$body .= "<td>{$model->telefone}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('email')}:</th>";
				$body .= "<td>{$model->email}</td>";
				$body .= "</tr>";
				$body .= "</table>";
				
				mail($from, 'Site > Anuncie', $body, $headers, "-r" . $from);
				Yii::app()->user->setFlash('contatoForm', 'Obrigado por entrar em contato. Responderemos sua mensagem o mais breve possível.');
				$this->refresh();
			}
		}
		
		$this->render('anuncie', array(
			'model' => $model
		));
	}

	public function actionCadastro() 
	{		
		$model = new ContatoForm('cadastro');
		if(isset($_POST['ContatoForm']))
		{
			$model->attributes=$_POST['ContatoForm'];
			if($model->validate())
			{
				$from = Yii::app()->params['emailContato'];
				$headers  = "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=utf-8\r\n";
				$headers .= "From: {$from}\r\n";
				$headers .= "Reply-To: {$model->email}\r\n";
				$headers .= "Return-Path: {$from}\r\n";
				$body  = "<style>";
				$body .= "table { width: 100%; font: normal 13px/20px Arial, sans-serif; }";
				$body .= "th, td { padding: 4px 8px; border-bottom: 1px solid #ddd; }";
				$body .= "th { width: 140px; text-align: left; }";
				$body .= "</style>";
				$body .= "<table>";
				$body .= "<tr>";
				$body .= "<th>{$model->getAttributeLabel('nome')}:</th>";
				$body .= "<td>{$model->nome}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('area')}:</th>";
				$body .= "<td>{$model->area}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('contato')}:</th>";
				$body .= "<td>{$model->contato}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('telefone')}:</th>";
				$body .= "<td>{$model->telefone}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('email')}:</th>";
				$body .= "<td>{$model->email}</td>";
				$body .= "</tr>";
				$body .= "</table>";
				
				mail($from, 'Site > Cadastro', $body, $headers, "-r" . $from);
				Yii::app()->user->setFlash('contatoForm', 'Obrigado por entrar em contato. Responderemos sua mensagem o mais breve possível.');
				$this->refresh();
			}
		}
		
		$this->render('cadastro', array(
			'model' => $model
		));
	}
	
	public function actionContato() 
	{		
		$model = new ContatoForm('contato');
		if(isset($_POST['ContatoForm']))
		{
			$model->attributes=$_POST['ContatoForm'];
			if($model->validate())
			{
				$from = Yii::app()->params['emailContato'];
				$headers  = "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=utf-8\r\n";
				$headers .= "From: {$from}\r\n";
				$headers .= "Reply-To: {$model->email}\r\n";
				$headers .= "Return-Path: {$from}\r\n";
				$body  = "<style>";
				$body .= "table { width: 100%; font: normal 13px/20px Arial, sans-serif; }";
				$body .= "th, td { padding: 4px 8px; border-bottom: 1px solid #ddd; }";
				$body .= "th { width: 140px; text-align: left; }";
				$body .= "</style>";
				$body .= "<table>";
				$body .= "<tr>";
				$body .= "<th>{$model->getAttributeLabel('nome')}:</th>";
				$body .= "<td>{$model->nome}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('contato')}:</th>";
				$body .= "<td>{$model->contato}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('telefone')}:</th>";
				$body .= "<td>{$model->telefone}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('email')}:</th>";
				$body .= "<td>{$model->email}</td>";
				$body .= "</tr><tr>";
				$body .= "<th>{$model->getAttributeLabel('mensagem')}:</th>";
				$body .= "<td>{$model->mensagem}</td>";
				$body .= "</tr>";
				$body .= "</table>";
				
				mail($from, 'Site > Contato', $body, $headers, "-r" . $from);
				Yii::app()->user->setFlash('contatoForm', 'Obrigado por entrar em contato. Responderemos sua mensagem o mais breve possível.');
				$this->refresh();
			}
		}
		
		$this->render('contato', array(
			'model' => $model
		));
	}
	
	public function actionError() 
	{
		if ($error = Yii::app()->errorHandler->error) 
		{
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}