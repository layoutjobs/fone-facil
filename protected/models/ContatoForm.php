<?php

class ContatoForm extends CFormModel
{
	public $nome;
	public $area;
	public $contato;
	public $telefone;
	public $email;
	public $mensagem;

	public function rules()
	{
		return array(
			array('nome, contato, telefone, email', 'required'),
			array('area', 'required', 'on' => 'anuncie, cadastro'),
			array('mensagem', 'required', 'on' => 'contato'),
			array('email', 'email'),
			array('mensagem', 'safe')
		);
	}

	public function attributeLabels()
	{
		return array(
			'nome' => 'Nome do Comércio, Indústria ou Profissional',
			'area' => 'Área de atuação',
			'contato' => 'Nome do contato',
			'telefone' => 'Telefone',
			'email' => 'E-mail',
			'mensagem' => 'Mensagem'
		);
	}
}