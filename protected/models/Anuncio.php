<?php

class Anuncio extends CActiveRecord
{
	public $s;
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return 'Anuncio';
	}
	
	public function rules()
	{
		return array(
			//array('titulo', 'required'),
			array('titulo, campo1Titulo, campo2Titulo, campo3Titulo, campo4Titulo, campo5Titulo, campo1Valor, campo2Valor, campo3Valor, campo4Valor, campo5Valor', 'length', 'max' => 64),
			array('img', 'file', 'types'=>'jpg, gif, png', 'allowEmpty' => true),
			array('conteudo', 'safe'),
			array('img', 'required', 'on' => 'tipo0'),
			array('titulo', 'required', 'on' => 'tipo1'),
			array('titulo, campo1Valor', 'required', 'on' => 'tipo2'),
			array('s', 'safe', 'on' => 'search')
		);
	}
	
	public function relations()
	{
		return array(
			'pagina' => array(self::BELONGS_TO, 'Pagina', 'paginaId'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => 'ID',
			'paginaId' => 'Pagina',
			'img' => 'Imagem',
			'titulo' => 'Título',
			'conteudo' => 'Conteúdo'
		);
	}
	
	public function isDestaque() {
		if ($this->tipo == 2) {
			$isDestaque = self::model()->find('tipo <> 2 AND titulo = :titulo', array(
				':titulo' => trim($this->titulo)
			));
		}
		return empty($isDestaque) ? false : true;
	}
	
	public function getImgUrl() {
		return $this->img ? Yii::app()->baseUrl . '/files/' . $this->img : '';		
	}
	
	public function getImgPath() {
		return $this->img ? Yii::app()->basePath . '/../files/' . $this->img : '';		
	}
	
	protected function beforeSave() {
		if (parent::beforeSave()) {
			
			// Verifica se o campo img recebeu algum upload.
			if (is_object($this->img)) {
				
				//Exclui a imagem antiga.
				if (is_file($this->imgPath)) unlink($this->imgPath);			
				
				// Define o nome do arquivo.					
				// $uploadedFileName = preg_replace('/[^a-z.]/', '', strtolower($this->img)); 
				$uploadedFileName = 'anuncio_' . $this->id . '.' . $this->img->extensionName;
				
				// Salva a imagem upada.
				$this->img->saveAs(Yii::app()->basePath . '/../files/' . $uploadedFileName);
				$this->img = $uploadedFileName;
			} else
				if (!is_file($this->imgPath)) 
					$this->img = null;
			
			return true;
		} else
			return false;
	}

	protected function afterDelete() {
		parent::afterDelete();
		
		if (is_file($this->imgPath)) unlink($this->imgPath);	
	}
		
	public function search()
	{
		$criteria = new CDbCriteria;
		
		$criteria->compare('titulo', $this->s, true, 'OR');
		
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'titulo',
			)
		));
	}
}