<?php

class Pagina extends CActiveRecord
{
	public $s;
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return 'Pagina';
	}
	
	public function rules()
	{
		return array(
			array('titulo, nome', 'required'),
			array('titulo', 'length', 'max' => 64),
			array('nome', 'unique'),
			array('conteudo', 'safe'),
			array('s', 'safe', 'on' => 'search')
		);
	}
	
	public function relations()
	{
		return array(
			'anuncios' => array(self::HAS_MANY, 'Anuncio', 'paginaId'),
		);
	}
	
	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => 'ID',
			'titulo' => 'Título',
			'conteudo' => 'Conteúdo'
		);
	}
	
	public function getUrl() {
		return Yii::app()->createUrl('/site/pagina', array('id' => $this->nome));
	}
		
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('titulo', $this->s, true, 'OR');
		$criteria->compare('nome', $this->s, true, 'OR');

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'nome'
			),
			'pagination' => array(
				'pageSize' => 20
			)
		));
	}
}