<?php

class Usuario extends CActiveRecord
{
	const NIVEL_ACESSO_ADMIN = 1;
	
	public $s;			
	public $novaSenha;
	public $novaSenhaRepeat;
		
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return 'Usuario';
	}
	
	public function rules()
	{
		return array(
			array(
				'nome, login',
				'required'
			),
			array(
				'nivelAcesso, ativo',
				'boolean'
			),
			array(
				'nome',
				'length',
				'max' => 64
			),
			array(
				'login',
				'length',
				'max' => 16
			),
			array(
				'login',
				'unique',
				'message' => 'Já existe um usuário com este login.'
			),
			array(
				'novaSenha, novaSenhaRepeat',
				'required',
				'on' => 'alterarSenha'
			),
			array(
				'novaSenha, novaSenhaRepeat',
				'length',
				'min' => 6
			),
			array(
				'novaSenha, novaSenhaRepeat',
				'length',
				'max' => 12
			),
			array(
				'novaSenhaRepeat', 
				'compare', 
				'compareAttribute' => 'novaSenha', 
				'message' => 'As senhas não correspondem.'
			),
			array(
				's',
				'safe',
				'on' => 'search'
			)
		);
	}
	
	public function relations()
	{
		return array(
		);
	}
	
	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => 'ID',
			'nivelAcesso' => 'Administrador',
			'novaSenha' => 'Nova senha',
			'novaSenhaRepeat' => 'Repita a nova senha',
		);
	}

	protected function gerarSalt()
	{
		return uniqid('', true);
	}
	
	public function validarSenha($senha)
	{
		return $this->hashCode($senha, $this->salt) === $this->senha;
	}
	
	public function hashCode($code, $salt)
	{
		return md5($salt . $code);
	}
	
	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			$this->login = strtolower(trim($this->login));
			
			if ($this->isNewRecord) {
				$this->salt  = $this->gerarSalt();
				$this->senha = $this->login;
				$this->senha = $this->hashCode($this->senha, $this->salt);
			}
			
			if (!empty($this->novaSenha))
				$this->senha = $this->hashCode($this->novaSenha, $this->salt);
			
			return true;
		} else
			return false;
	}
	
	public function search()
	{
		$criteria = new CDbCriteria;
		
		$criteria->compare('nome', $this->s, true);
		$criteria->compare('login', $this->s, true);
		
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria
		));
	}
}